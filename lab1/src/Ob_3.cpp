#include "Ob_3.h"

#include <iostream>
using namespace std;

Ob_3 :: Ob_3 () {}

Ob_3 :: Ob_3 (string name) {
    this -> ob_name = name;
}

void Ob_3 :: showChildrensNames () {
    cout << "        " << ob_name << endl;
}
