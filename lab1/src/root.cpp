#include "Root.h"
#include "Ob_1.h"
#include "Ob_2.h"

#include <iostream>
using namespace std;

Root::Root() {
   this -> ob_name = "Root";
}


void Root :: buildTree () {
    string s = "ob_1";

    Ob_1 * obj = new Ob_1 (s);
    this -> _children.push_back(obj);

    for (int i = 1; i < 4; i++) {
        string s = "ob_x";
		s[3] = i + 1 + '0';

        Ob_1 * obj = new Ob_1 (s);
        obj -> buildTree();
        this -> _children.push_back(obj);
    }
}


void Root :: showChildrensNames() {
    cout << ob_name << endl;

    for (it = _children.begin(); it != _children.end(); it++) {
       (*it) -> showChildrensNames();
    }
}
