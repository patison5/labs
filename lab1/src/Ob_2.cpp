#include "Ob_2.h"
#include "Ob_3.h"

#include <iostream>
using namespace std;

Ob_2 :: Ob_2 () {}
Ob_2 :: Ob_2 (string name) {
    this -> ob_name = name;
}


void Ob_2 :: buildTree () {
    for (int i = 0; i < 1; i++) {
        string s = ob_name + "_ob_x";
		s[13] = i + 1 + '0';

        Ob_3 * obj = new Ob_3 (s);
        this -> _children.push_back(obj);
    }
}

void Ob_2 :: showChildrensNames () {
    cout << "      " << ob_name << endl;

    for (it = _children.begin(); it != _children.end(); it++) {
       (*it) -> showChildrensNames();
    }

    cout << endl;
}
