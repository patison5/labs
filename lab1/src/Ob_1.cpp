#include "Ob_1.h"
#include "Ob_2.h"

#include <iostream>
using namespace std;

Ob_1 :: Ob_1 () {}

Ob_1 :: Ob_1(string name) {
    this -> ob_name = name;
}


void Ob_1 :: buildTree () {
    for (int i = 0; i < 2; i++) {
        string s = ob_name + "_ob_x";
		s[8] = i + 1 + '0';

        Ob_2 * obj = new Ob_2 (s);
        (*obj).buildTree();
        _children.push_back(obj);
    }
}

void Ob_1 :: showChildrensNames () {
    cout << "  " << ob_name << endl;

    for (it2 = _children.begin(); it2 != _children.end(); it2++) {
       (*it2) -> showChildrensNames();
    }
}

