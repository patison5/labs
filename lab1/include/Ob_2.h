#ifndef OB_2_H
#define OB_2_H

#include <Ob_1.h>

class Ob_3;

class Ob_2 : public Ob_1 {
    public:
        Ob_2 ();
        Ob_2 (string name);

        void buildTree();

        void showChildrensNames();

    private:
        vector <Ob_3 *> _children;
        vector <Ob_3 *> :: iterator it;
};

#endif // OB_2_H
