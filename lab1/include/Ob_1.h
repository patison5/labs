#ifndef OB_1_H
#define OB_1_H

#include <Root.h>

class Ob_2;

class Ob_1  : public Root {
    public:
        Ob_1();
        Ob_1(string name);

        void showChildrensNames();

        void buildTree();

    private:
        vector <Ob_2 *> _children;
        vector <Ob_2 *> :: iterator it2;
};

#endif // OB_1_H
