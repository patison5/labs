#ifndef OB_3_H
#define OB_3_H

#include <Ob_2.h>


class Ob_3 : public Ob_2 {
    public:
        Ob_3();
        Ob_3 (string name);

        void showChildrensNames();

    private:

};

#endif // OB_3_H
