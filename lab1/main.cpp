#include <iostream>

using namespace std;


#include "root.h"

int main(){

    Root root;

    root.buildTree();

    root.showChildrensNames();

    return 0;
}
