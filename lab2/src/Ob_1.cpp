#include "Ob_1.h"
#include "Ob_2.h"

#include <iostream>
using namespace std;

Ob_1 :: Ob_1 () {}

Ob_1 :: Ob_1(string name) {
    this -> ob_name = name;
}


void Ob_1 :: buildTree () {
    for (int i = 0; i < 5; i++) {
        string s = "ob_1_ob_x";
		s[8] = i + 1 + '0';

        Ob_2 * obj = new Ob_2 (s);
        _children.push_back(obj);
    }
}

void Ob_1 :: showChildrensNames () {
    cout << "  " << ob_name << endl;

    for (it2 = _children.begin(); it2 != _children.end(); it2++) {
       (*it2) -> showChildrensNames();
    }
}



bool Ob_1 :: checkExistance (string str, string tempStr) {
    tempStr = tempStr + "/" + ob_name;

    if (str == tempStr) {
        return true;
    } else {
        bool temp;

        for (it2 = _children.begin(); it2 != _children.end(); it2++) {

            temp = (*it2) -> checkExistance(str, tempStr);

            if (temp)
                break;
        }

        return temp;
    }
}
