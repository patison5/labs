#include "Ob_2.h"

#include <iostream>
using namespace std;

Ob_2 :: Ob_2 () {}
Ob_2 :: Ob_2 (string name) {
    this -> ob_name = name;
}

void Ob_2 :: showChildrensNames () {
    cout << "      " << ob_name << endl;
}


bool Ob_2 :: checkExistance (string str, string tempStr) {
    tempStr = tempStr + "/" + ob_name;

    if (str == tempStr)
        return true;
    else
        return false;
}

//Root/ob_2/ob_1_ob_1
