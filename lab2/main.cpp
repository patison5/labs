#include <iostream>

using namespace std;


#include "root.h"

int main(){

    Root root;

    root.buildTree();

    root.showChildrensNames();

    while (true) {
        string str;
        cout << "\n\nWrite string: " ;
        cin >> str;

        (root.checkExistance(str)) ? cout << "Exist! \n" : cout << "Not exist! \n";
    }

    return 0;
}
