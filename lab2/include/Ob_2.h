#ifndef OB_2_H
#define OB_2_H

#include <Ob_1.h>


class Ob_2 : public Ob_1 {
    public:
        Ob_2 ();
        Ob_2 (string name);

        void showChildrensNames();

        bool checkExistance (string str, string tempStr);

    private:
        vector <Ob_1 *> _children;
        vector <Ob_1 *> :: iterator it;
};

#endif // OB_2_H
