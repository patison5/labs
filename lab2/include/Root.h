#ifndef ROOT_H
#define ROOT_H

#include <vector>
#include <string>

using namespace std;

class Ob_1;

class Root {
    public:
        Root();
        string ob_name;

        void buildTree();

        void showChildrensNames();

        bool checkExistance (string str);

    private:
        vector <Ob_1 *> _children;
        vector <Ob_1 *> :: iterator it;
};

#endif // ROOT_H
