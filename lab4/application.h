#ifndef CL_APPLICATION_H
#define CL_APPLICATION_H


#include "cl_2.h"
#include "cl_3.h"
#include "cl_p3.h"


class cl_application : public cl_base {
public:
    cl_application ( );


    void bild_tree_objects ( );
    int  exec_app          ( );

    void signal_to_ob_3 ( string s_command )  { }

private:
    cl_base * p_ob;
    cl_3    * p_ob_3;
};

#endif // CL_APPLICATION_H
