
#include "cl_base.h"
<...>
void cl_base :: set_connect ( void ( * s_ignal ) ( string ), void ( * h_endler ) ( string ) )
{
    //-------------------------------------------------------------------------
    // Определение связи сигнал - слот
    // Сигнал от текущего документа.
    // Передается строка сообщения..
    // Переделать на динамическую последовательность парамтеров.
    //-------------------------------------------------------------------------

    connects.insert ( { s_ignal, h_endler } );
}

void cl_base :: delete_connect ( void ( * s_ignal ) ( string ), void ( * h_endler ) ( string ) )
{
    //-------------------------------------------------------------------------
    // Разрыв связи сигнал - слот
    // Сигнал от текущего документа.
    //-------------------------------------------------------------------------

    if ( connects.empty ( )              ) return;
    if ( connects.count ( s_ignal ) == 0 ) return;

    it_connect = connects.find ( s_ignal );

    while ( it_connect != connects.end ( ) ) {

        if ( ( it_connect -> first  ) == s_ignal  &&
             ( it_connect -> second ) == h_endler    ) {

            connects.erase ( it_connect );

            return;
        }

        it_connect ++;
    }
}
void cl_base :: emit_signal ( void ( * s_ignal ) ( string ), string s_command )
{
    //-------------------------------------------------------------------------
    // Отправка сигнала объектам согласно установленным связам
    //-------------------------------------------------------------------------
    void ( * p_slot ) ( string );
    //-------------------------------------------------------------------------

    if ( connects.empty ( )              ) return;
    if ( connects.count ( s_ignal ) == 0 ) return;

    it_connect = connects.find ( s_ignal );

    while ( it_connect != connects.end ( ) ) {

        if ( ( it_connect -> first ) == s_ignal ) {

            p_slot = it_connect -> second;

            ( p_slot ) ( s_command );
        }

        it_connect ++;
    }
}
