#ifndef TREATMENT_H
#define TREATMENT_H

#include <Root.h>


class Treatment : public Root {
    public:
        Treatment();

        void puzir (int * arr);
        void puzir2 (int * arr);

        void inversion(int * arr);

        int maxNumber (int * arr);

        void sortByChet(int * arr);

    protected:

    private:
};

#endif // TREATMENT_H
