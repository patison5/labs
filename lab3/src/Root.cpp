#include "Root.h"

#include "Conclusion.h"
#include "Treatment.h"
#include "Input.h"

#include <iostream>

using namespace std;

Root::Root() {

    vvod();
    (*treatment).puzir(mas);
    vivod();
    (*treatment).puzir2(mas);
    vivod();
    (*treatment).inversion(mas);
    vivod();
    (*treatment).sortByChet(mas);
    vivod();

    cout << "Max Number: " << (*treatment).maxNumber(mas);

}


void Root :: vivod () {
    (*conclusion).vivod(mas);
}
void Root :: vvod () {
    (*input).vvod(mas);
}
